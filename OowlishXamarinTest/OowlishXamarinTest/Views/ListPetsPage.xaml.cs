﻿using OowlishXamarinTest.Models;
using OowlishXamarinTest.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OowlishXamarinTest.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListPetsPage : ContentPage
    {
        public int Cont { get; set; } = 1;
        public ListPetsPage()
        {
            InitializeComponent();
        }

        private void navigationDrawerList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var content = (sender as ListView).SelectedItem as PetsDescription;
            (BindingContext as ListPetsPageViewModel)?.NavigateToDetailPage.Execute(content);
        }

        private void LoadInformations()
        {
            //var listPets = ((ListPetsPageViewModel)BindingContext).GetAllPets;
            //listPets.Execute(null);
            var list = ((App)Application.Current).Connection.Query<PetsDescription>("select * from PetsDescription");
            navigationDrawerList.ItemsSource = list;

        }
        private void BtnAddItem_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var image = "dog.jpg";
                var query = $"insert into PetsDescription (Name, Picture, Breed, Weight, Description, Latitude, Longitude) values ('name Pet{Cont}', '{image}', 'breed Pet{Cont}', 'weight Pet{Cont}', 'description Pet{Cont}', '-3.8502537', '-38.4000899')";

                ((App)Application.Current).Connection.Execute(query);
                Cont += 1;
                LoadInformations();

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        protected override void OnAppearing()
        {
            LoadInformations();
            base.OnAppearing();

        }
    }
}
