﻿using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace OowlishXamarinTest.Views
{
    public partial class DetailPetPage : ContentPage
    {
        public DetailPetPage()
        {
            InitializeComponent();
            LocationMaps.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-3.8502537, -38.4000899), Distance.FromMiles(0.5)));

            var pin = new Pin()
            {
                Label = "Oowlish",
                Position = new Position(-3.8502537, -38.4000899),
                Type = PinType.Place
            };

            LocationMaps.Pins.Add(pin);
        }
    }
}
