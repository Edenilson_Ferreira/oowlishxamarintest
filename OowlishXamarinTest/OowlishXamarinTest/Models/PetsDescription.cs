﻿using SQLite;

namespace OowlishXamarinTest.Models
{
    public class PetsDescription
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Breed { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

}
