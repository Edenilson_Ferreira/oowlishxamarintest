﻿using OowlishXamarinTest.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Prism.Navigation;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace OowlishXamarinTest.ViewModels
{
    public class DetailPetPageViewModel : ViewModelBase
    {
        private ImageSource _image;

        public ImageSource Image
        {
            get => _image;
            set => SetProperty(ref _image, value);
        }

        private Pin _pinLocate;

        public Pin PinLocate
        {
            get => _pinLocate;
            set => SetProperty(ref _pinLocate, value);
        }

        private PetsDescription _pet;
        public PetsDescription Pet
        {
            get => _pet;
            set => SetProperty(ref _pet, value);
        }

        public Command TakePhoto { get; set; }

        public DetailPetPageViewModel(PetsDescription pet, INavigationService navigationService)
            : base(navigationService)
        {
            Pet = pet;
            Title = "Detail";
            TakePhoto = new Command(ExecuteTakePhotoCommand);
        }

        private async void ExecuteTakePhotoCommand()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await App.Current.MainPage.DisplayAlert("Sem Câmera", ":( Nenhuma câmera está disponível.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "Profile",
                Name = String.Format("{0}.jpg", DateTime.Now).Trim(),
                SaveToAlbum = true,
                CompressionQuality = 92,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000
            });

            if (file == null)
                return;
            await App.Current.MainPage.DisplayAlert("Localização do arquivo", file.Path, "OK");

            Image = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                return stream;
            });

            Pet.Picture = file.Path;
            try
            {
                UpdateItem(Pet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public int UpdateItem(PetsDescription pet)
        {
            //var list = ((App)Application.Current).Connection.Query<PetsDescription>("select * from pets");
            var list = ((App)Application.Current).Connection.Update(pet);
            return list;
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("pet"))
            {
                var model = (PetsDescription)parameters["pet"];
                Pet = model;
            }
            else
            {
                App.Current.MainPage.DisplayAlert("Chave não encontrada", ":/", "Ok");
            }
        }
    }
}
