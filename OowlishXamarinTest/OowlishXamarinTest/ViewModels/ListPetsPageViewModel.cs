﻿using OowlishXamarinTest.Models;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace OowlishXamarinTest.ViewModels
{
    public class ListPetsPageViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        public ObservableCollection<PetsDescription> Pets { get; set; } = new ObservableCollection<PetsDescription>();
        public static List<PetsDescription> ListViewItems { get; set; } = new List<PetsDescription>();
        public Command NavigateToDetailPage { get; }
        public Command GetAllPets { get; }

        private DelegateCommand<ItemTappedEventArgs> _goToDetailPage;
        public DelegateCommand<ItemTappedEventArgs> GoToDetailPage
        {
            get
            {
                if (_goToDetailPage == null)
                {
                    _goToDetailPage = new DelegateCommand<ItemTappedEventArgs>(async selected =>
                    {
                        NavigationParameters param = new NavigationParameters();
                        param.Add("model", selected.Item);
                        await _navigationService.NavigateAsync("DetailPetPage", param);
                    });
                }
                return _goToDetailPage;
            }
        }

        public ListPetsPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            _navigationService = navigationService;
            _navigationService = navigationService;
            Title = "List of Pets";
            NavigateToDetailPage = new Command<PetsDescription>(ExecuteNavigateDetailPage);
            GetAllPets = new Command(ExecuteGetAllPets);
        }

        public void ExecuteGetAllPets()
        {
            try
            {
                var list = ((App)Application.Current).Connection.Query<PetsDescription>("select * from PetsDescription");
                foreach (var pet in list)
                {
                    Pets.Add(pet);
                }

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }
        private async void ExecuteNavigateDetailPage(PetsDescription pet)
        {
            var param = new NavigationParameters();
            param.Add("pet", pet);
            await _navigationService.NavigateAsync("DetailPetPage", param);
        }
    }
}
