﻿using OowlishXamarinTest.Models;
using OowlishXamarinTest.Views;
using PCLExt.FileStorage;
using PCLExt.FileStorage.Folders;
using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace OowlishXamarinTest
{
    public partial class App : PrismApplication
    {
        public SQLiteConnection Connection { get; private set; }
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            var folder = new LocalRootFolder();
            var db = folder.CreateFile("database.db", CreationCollisionOption.OpenIfExists);
            Connection = new SQLiteConnection(db.Path);
            Connection.CreateTable<PetsDescription>();
            await NavigationService.NavigateAsync("NavigationPage/ListPetsPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<ListPetsPage>();
            containerRegistry.RegisterForNavigation<DetailPetPage>();
        }
    }
}
