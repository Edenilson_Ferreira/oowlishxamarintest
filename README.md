# Oowlish - Xamarin Test

# Libs/Frameworks :
  > - [Prism Template Pack](https://marketplace.visualstudio.com/items?itemName=BrianLagunas.PrismTemplatePack) - Framework MVVM
  > - [Xam.Plugin.Media](https://www.nuget.org/packages/Xam.Plugin.Media/3.1.3) - Take photo
  > - [SQLite](https://www.nuget.org/packages/sqlite-net-pcl/1.4.118) - Database
  > - [PCLExt.FileStorage](https://www.nuget.org/packages/PCLExt.FileStorage/) - FileStorage
  > - [Xamarin.Forms.Maps](https://www.nuget.org/packages/Xamarin.Forms.Maps/2.5.0.280555) - Maps
  
# How to install
  > - Prism Template Pack - https://marketplace.visualstudio.com/items?itemName=BrianLagunas.PrismTemplatePack
  > - Xam.Plugin.Media - (.Net CLI) `dotnet add package Xam.Plugin.Media --version 3.1.3`
  > - SQLite - (.Net CLI) `dotnet add package sqlite-net-pcl --version 1.4.118`
  > - PCLExt.FileStorage - (.Net CLI) `dotnet add package PCLExt.FileStorage --version 1.3.2`
  > - Xamarin.Forms.Maps - (.Net CLI) `dotnet add package Xamarin.Forms.Maps --version 2.5.0.280555`
  
  
  

